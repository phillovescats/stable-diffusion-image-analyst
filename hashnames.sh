#!/bin/bash

# Navigate to the img directory
cd img || exit

# Loop through each PNG file in the directory
for file in *.png; do
    # Calculate the MD5 hash of the file
    md5=$(md5sum "$file" | cut -d ' ' -f 1)
    
    # Rename the file with the MD5 hash as the new name
    mv "$file" "${md5}.png"
done
