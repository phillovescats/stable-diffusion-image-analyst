# Stable Diffusion Image Analyst

**IMPORTANT:** This project is in an early state and still under development.


## Table of contents

1. Introduction
2. Requirements\
   2.1 Install Git\
   2.2 Install Python (3.10.6)\
   2.3 Optional: Install PyEnv (Linux only)
3. Run it\
   3.1 Windows\
   3.2 Linux
4. Troubleshooting
5. License


## 1. Introduction

The Stable Diffusion Image Analyst is a simple tool to work on .png image files that got generated using Stable 
Diffusion. Nevertheless, is it completely independent of Stable Diffusion and works also for other .png images.

**The functionality of this project is expressed as follows:**  
- Creating a virtual python environment and set up every requirement
- Renaming the given image files with their hash value
- Tagging of the given image files using [sd-scripts](https://github.com/kohya-ss/sd-scripts)
- Definition of tags in a persistence.txt, which are extended to each tag file
- Extract metadata from with Stable Diffusion generated images

**Limitations:**
- The image tagging is hardcoded for non-realistic images yet
- bash respectively batch is used to rename the files

This project has been successfully tested on Windows 11 and Debian 12 (Bookworm). 


## 2. Requirements

- A modern Windows or Linux operating system installation
- Git
- Python Version **3.10.6**
- Around 5 GB free space

### 2.1 Install Git

If you're on a linux distribution that supports the apt package manager, you can install git with:

```
sudo apt update && sudo apt install git -y
```

Alternatively you can download Git for your OS [here](https://git-scm.com/downloads)

### 2.2 Install Python (3.10.6)
\
**ATTENTION!** 

It is necessary to run exactly this version of Python (3.10.6) and not an older or newer one!
In any other case you risk to have package inconsistencies which can lead to a broken program.

In most cases, you do not need to delete your current Python installation because of a possible coexistence.
I recommend to install the correct version from the [official sources](https://www.python.org/downloads/release/python-3106/)

### 2.3 Optional: Install PyEnv (Linux only)

If you are not sure how to run multiple versions of python in the same system, you can use pyenv. 
Be aware of the fact that this is not supported in Windows! 

Here is a quick guide to do so:

1. Install the requirements of pyenv:
```
sudo apt update && sudo apt install -y make build-essential libssl-dev zlib1g-dev \
 libbz2-dev libreadline-dev libsqlite3-dev wget curl llvm libncurses5-dev \
 libncursesw5-dev xz-utils tk-dev libffi-dev liblzma-dev
```
2. Download and install the program from its repository:
```
curl -L https://raw.githubusercontent.com/pyenv/pyenv-installer/master/bin/pyenv-installer | bash
```
3. Let PyEnv install Pyython 3.10.6 for you:
```
pyenv install 3.10.6
```
4.  Now you can switch into the local installation of the new PyEnv environment:
```
pyenv local 3.10.6
```
5. You also have to add PyEnv to your .bashrc:
```
{
   echo "# Autoload pyenv for bash
   export PYENV_ROOT='$HOME/.pyenv'
   [[ -d $PYENV_ROOT/bin ]] && export PATH='$PYENV_ROOT/bin:$PATH'
   eval '$(pyenv init -)'"
} >> ~/.bashrc
```

That's it! You can find your PyEnv installation under following path: `~/.pyenv`


## 3. Run it

After fulfilling the requirements, clone this project with:

```
git clone [LINK_TO_THIS_PROJECT]
```

Then, put the images you want to be analyzed in the /img subdirectory.

### 3.1 Windows

Now open a Windows CMD or PowerShell in the project directory and execute this command:

```
./run.bat
```

Depending on your internet connection and cpu power the first run could take 10 - 30 minutes.

After the installation of the requirements is done the program will start and operate way faster the next times!


### 3.2 Linux

First, you have to mark the shell scripts file as executables using:

```
chmod u+x hashnames.sh
chmod u+x run.sh
```

After that you can start the program using:

```
./run.sh
```


## 4. Troubleshooting

- Sometimes a simple re-run helps

- If sd-scripts have trouble to find any resources, you can let them force-download them by the script with deleting 
  any occurrences of `venv1` and/or `venv2` in the project root directory.

**Windows only:**

- If there is not a valid Python Version 3.10.6 installation in the standard path, you have to create a virtual 
  environment by yourself. A simple solution is to call `PATH\TO\MY\Python310\python.exe -m venv venv` while in the 
  project folder.

**Linux only:**

- If you use PyEnv as environment manager and experience problems with dependencies, try to delete the whole `~/.pyenv` 
  instance and redo step 2.3 


## 5. License

This project is licensed under the **MIT** license, as far as it do not interfere with licenses from the tools that get 
leveraged here. 

PIL from the [Pillow](https://github.com/python-pillow/Pillow) project is licensed under the open source **HPND** 
License

The majority of scripts of [sd-scripts](https://github.com/kohya-ss/sd-scripts) is licensed under **ASL 2.0** (including 
codes from Diffusers, cloneofsimo's and LoCon), however portions of the project are available under separate license 
terms:

[Memory Efficient Attention Pytorch](https://github.com/lucidrains/memory-efficient-attention-pytorch): **MIT**

[bitsandbytes](https://github.com/TimDettmers/bitsandbytes): **MIT**

[BLIP](https://github.com/salesforce/BLIP): **BSD-3-Clause**
