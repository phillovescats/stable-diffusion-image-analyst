#!/bin/python

import os
import tagging
import alter
import platform

root = os.getcwd()
if platform.system() == 'Windows':
    img = "{}\img".format(root)
elif platform.system() == 'Linux':
    img = "{}/img".format(root)


def main():
    if platform.system() == 'Windows':
        os.system("hashnames.bat")
    elif platform.system() == 'Linux':
        os.system("./hashnames.sh")
    else:
        raise SystemExit("Running on an unsupported operating system")

    tagging.art_tags()

    alter.process_txt_files_in_directory(img, root)

    import pil
    pil.process_txt_files_in_directory(img)

    print("main.py finished.")


if __name__ == "__main__":
    main()
