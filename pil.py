#!/bin/python

import os
import fileinput
from PIL import Image


def extract_metadata(file_path):
    with fileinput.FileInput(file_path, inplace=True) as file:
        new_name = "{}.meta.txt".format(file_path)
    
        im = Image.open(file_path)
        
        im.load()
        
        with open(new_name, "w") as file:
            if im.info.get('parameters'):
                file.write(str(im.info['parameters']))
            else:
                # file.write(str('No parameters found.'))
                print('There are no parameters in {}'.format(file_path))


def process_txt_files_in_directory(directory):
    for filename in os.listdir(directory):
        if filename.endswith(".png"):
            file_path = os.path.join(directory, filename)
            extract_metadata(file_path)
