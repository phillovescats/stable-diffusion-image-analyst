#!/bin/bash

if command -v pyenv &>/dev/null; then
    pyenv local 3.10.6
else
    if [ -d "venv" ]; then
        source /venv/bin/activate
    else
        python -m venv venv
        source /venv/bin/activate
    fi
fi

python_version=$(python3 --version 2>&1 | awk '{print $2}')

if [ "$python_version" == "3.10.6" ]; then
    echo "Python version is 3.10.6"
    python3 -m pip install -r ./requirements.txt
	  python3 main.py
else
    echo "Python version is not 3.10.6" 1>&2
	exit 127
fi
