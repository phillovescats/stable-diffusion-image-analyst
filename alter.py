#!/bin/python

import os
import platform
import fileinput


def replace_spaces_with_underscore(file_path):
    with fileinput.FileInput(file_path, inplace=True) as file:
        for line in file:
            print(line.replace(" ", "_"), end='')


def replace_comma_underscore_with_space(file_path):
    with fileinput.FileInput(file_path, inplace=True) as file:
        for line in file:
            print(line.replace(",_", " "), end='')


def add_persistence_tags(file_path, root):
    if platform.system() == 'Windows':
        pers = "{}\\persistence.txt".format(root)
    elif platform.system() == 'Linux':
        pers = "{}/persistence.txt".format(root)
    
    if os.path.exists(pers):
        with open(pers, "r") as pers_file:
            persistence_content = pers_file.read().strip()
        
        if os.path.exists(file_path):
            with open(file_path, "r") as file_path_file:
                existing_content = file_path_file.read().strip()
        else:
            existing_content = ""
        
        combined_content = existing_content + ' ' + persistence_content
        
        with open(file_path, "w") as file_path_file:
            file_path_file.write(combined_content)
        
        print("Content of persistence.txt added to the first line of", file_path)
        
    else:
        print("File persistence.txt does not exist")


def process_txt_files_in_directory(directory, root):
    for filename in os.listdir(directory):
        if filename.endswith(".txt"):
            file_path = os.path.join(directory, filename)
            replace_spaces_with_underscore(file_path)
            replace_comma_underscore_with_space(file_path)
            add_persistence_tags(file_path, root)
