@echo off
setlocal enableextensions disabledelayedexpansion

CD /D img

for /f "tokens=1,* delims=:" %%a in ('
    cmd /q /c "for %%f in (*.png) do certutil -hashfile "%%f" MD5&&(echo file:%%f)"
    ^| findstr /v /b /i /c:"MD5" /c:"Cert"
') do (
    if "%%a"=="file" (
        set "file=%%b"
        set "extension=%%~xb"
        setlocal enabledelayedexpansion
            move "!file!" "!md5: =!!extension!"
        endlocal
        set "md5="
    ) else (
        set "md5=%%a"
    )
)