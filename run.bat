@echo off

if exist "venv" (
    call venv\Scripts\activate
) else (
    call %APPDATA%\..\Local\Programs\Python\Python310\python.exe -m venv venv
	call venv\Scripts\activate
)

for /f "delims=" %%v in ('python --version 2^>^&1') do set "python_version=%%v"

for /f "tokens=2" %%n in ("%python_version%") do set "version_number=%%n"

if "%version_number%" == "3.10.6" (
    echo Python version is 3.10.6
    python -m pip install -r requirements.txt
	python main.py
) else (
	call :getError
    echo Python version is not 3.10.6
)
