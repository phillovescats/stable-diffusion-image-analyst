#!/bin/python

import os
import sys
import platform

root = os.getcwd()
if platform.system() == 'Windows':
  img = os.getcwd() + "\\img"
  kohya = os.getcwd() + "\\kohya-trainer"
  venv1 = os.getcwd() + "\\venv1"
  venv2 = os.getcwd() + "\\venv2"
elif platform.system() == 'Linux':
  img = os.getcwd() + "/img"
  kohya = os.getcwd() + "/kohya-trainer"
  venv1 = os.getcwd() + "/venv1"
  venv2 = os.getcwd() + "/venv2"


def init():
  if not os.path.exists(img):
    os.makedirs(img)
  
  if not os.path.exists(kohya):
    os.makedirs(kohya)

  if not os.listdir(kohya):
    os.system('git clone https://github.com/kohya-ss/sd-scripts "{}"'.format(kohya))
    os.chdir(kohya)
    os.system("git reset --hard 5050971ac687dca70ba0486a583d283e8ae324e2")
    os.chdir(root)


def art_tags():
  init()

  # Recommended values are 0.35 (high sensitive = more tags) to 0.5 (less sensitive = less tags)
  tag_threshold = 0.4

  os.chdir(kohya)
  sys.path.append('{}'.format(kohya))  

  if not os.path.exists(venv1):
    print("\nInstalling dependencies if necessary...\n")
    os.system('pip install -r "{}"/requirements.txt'.format(kohya))
    
    print("\nTagging art...\n")
  
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
  
    os.system("python {}/finetune/tag_images_by_wd14_tagger.py \
      {} \
      --repo_id=SmilingWolf/wd-v1-4-swinv2-tagger-v2 \
      --model_dir={} \
      --thresh={} \
      --batch_size=8 \
      --caption_extension=.txt \
      --force_download".format(kohya, img, kohya, tag_threshold))
    
    with open(venv1, "w") as file:
        pass
        
  else:
    print("\nTagging art...\n")
  
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
  
    os.system("python {}/finetune/tag_images_by_wd14_tagger.py \
      {} \
      --repo_id=SmilingWolf/wd-v1-4-swinv2-tagger-v2 \
      --model_dir={} \
      --thresh={} \
      --batch_size=8 \
      --caption_extension=.txt".format(kohya, img, kohya, tag_threshold))

  print("\nArt tagged!\n")

    
def photo_tags():
  init()
    
  # Minimum amount of tokens for caption (10 recommended)
  caption_min = 10
  # Maximum amount of tokens for caption (75 recommended)
  caption_max = 75

  os.chdir(kohya)
  sys.path.append('{}'.format(kohya))  

  if not os.path.exists(venv2):
    print("\nInstalling dependencies if necessary...\n")
    os.system('pip install -r "{}"/requirements.txt'.format(kohya))
    
    print("\nTagging photos...\n")
  
    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
  
    os.system('python "{}"/finetune/make_captions.py \
      "{}" \
      --beam_search \
      --max_data_loader_n_workers=2 \
      --batch_size=8 \
      --min_length={} \
      --max_length={} \
      --caption_extension=.txt \
      --force_download'.format(kohya, img, caption_min, caption_max))
    
    with open(venv2, "w") as file:
      pass
    
  else:
    print("\nTagging photos...\n")

    os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'

    os.system('python "{}"/finetune/make_captions.py \
      "{}" \
      --beam_search \
      --max_data_loader_n_workers=2 \
      --batch_size=8 \
      --min_length={} \
      --max_length={} \
      --caption_extension=.txt'.format(kohya, img, caption_min, caption_max))

  print("\nPhotos tagged!\n")
